// Require objects.
var express = require('express');
var app     = express();
var aws     = require('aws-sdk');
var fs = require('fs');
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Edit this with YOUR email address.
var email   = "joules.mercurio@gmail.com";
    
// Load your AWS credentials and try to instantiate the object.
aws.config.loadFromPath(__dirname + '/config.json');


//allow origin access


app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });


// Instantiate SES.
var ses = new aws.SES();

// Verify email addresses.
app.get('/verify', function (req, res) {
    var params = {
        EmailAddress: email
    };
    
    ses.verifyEmailAddress(params, function(err, data) {
        if(err) {
            res.send(err);
        } 
        else {
            res.send(data);
        } 
    });
});

// Listing the verified email addresses.
app.get('/list', function (req, res) {
    ses.listVerifiedEmailAddresses(function(err, data) {
        if(err) {
            res.send(err);
        } 
        else {
            res.send(data);
        } 
    });
});

// Deleting verified email addresses.
app.get('/delete', function (req, res) {
    var params = {
        EmailAddress: email
    };

    ses.deleteVerifiedEmailAddress(params, function(err, data) {
        if(err) {
            res.send(err);
        } 
        else {
            res.send(data);
        } 
    });
});

// Sending  email 
app.post('/send', function (req, res) {

    if (!req.body.correo) {
        respuesta = {
            error: true,
            codigo: 502,
            mensaje: 'Falta el parametro correo.'
        };
        res.status(404).send(respuesta);
    }else{
        emailto = req.body.correo;

    var ses_mail = "From: 'Julio Ortiz' <" + email + ">\n";
    ses_mail = ses_mail + "To: " + emailto + "\n";
    ses_mail = ses_mail + "Subject: Envio de correo Julio Ortiz\n";
    ses_mail = ses_mail + "MIME-Version: 1.0\n";
    ses_mail = ses_mail + "Content-Type: multipart/mixed; boundary=\"NextPart\"\n\n";
    ses_mail = ses_mail + "--NextPart\n";
    ses_mail = ses_mail + "Content-Type: text/html; charset=us-ascii\n\n";
    let plantilla = fs.readFileSync('./html/3-validacion-cuenta.html', 'utf-8');
    ses_mail = ses_mail + plantilla;



    var eParams = {
        Destination: {
            ToAddresses: [emailto]
        },
        Message: {
            Body: {
                Html: {
                    Charset: "UTF-8", 
                    Data: plantilla
                   },
                Text: {
                    Data: "Hola"
                }
            },
            Subject: {
                Data: "Julio Ortiz- Envio de correo"
            }
        },
        Source: email
 };
    
    ses.sendEmail(eParams, function(err, data) {
        if(err) {
            res.send(err);
        } 
        else {
            data.mensaje = "Envio de correo exitoso.";
            res.send(data);
        }           
    });

    }
    
    
});




// Start server.
var server = app.listen(8080, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log(' app listening at http://%s:%s', host, port);
});
